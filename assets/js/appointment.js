(function($) {
  $(document).ready(function() {
    $(window).scroll(function(){
      var scroll = $(window).scrollTop();
      if( scroll > $(window).height()  / 2) {
        $('.appointment').removeClass('expand');
      }
      else {
        $('.appointment').addClass('expand');
      }
    });
  });
})(jQuery);
