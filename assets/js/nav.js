(function($) {
  $(document).ready(function() {
    resizeHero();
    $(window).resize(function(){
      resizeHero();
    });
    $(window).scroll(function(){
      var scroll = $(window).scrollTop();
      if( scroll > 1 ) {
        $('.navbar-brand img').removeClass('initial');
        resizeHero();
      }
      else if ( scroll === 0 ){
        $('.navbar-brand img').addClass('initial');
        resizeHero();
      }
    });
  });
  function resizeHero() {
    return;
    var navHeight = $('nav.fixed-top').outerHeight();
    var addressHeight = $('.address-bar').height();
    var windowHeight = $(window).height();
    jQuery('.hero').height(windowHeight-navHeight-addressHeight);

    //console.log( navHeight );
    //console.log( addressHeight );
    //console.log( windowHeight );
    //console.log( windowHeight-navHeight-addressHeight );

    //var heroHeight = jQuery('.hero').height()
    //console.log( heroHeight );
  }
})(jQuery);
