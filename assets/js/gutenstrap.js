var el = wp.element.createElement,
    registerBlockType = wp.blocks.registerBlockType,
    RichText = wp.blocks.RichText;

registerBlockType( 'bootstrap/testimonial', {
    title: 'Testimonial',
    description: 'Block showing a testimonial.',
    category: 'layout',
    icon: 'format-quote',
    attributes: {
      content: {
        type: 'array',
        selector: '.testimonial-content',
      },
      cite: {
        type: 'array',
        selector: '.testimonial-author',
      }
    },

    edit: function( props ) {
      var content = props.attributes.content;
      var cite = props.attributes.cite;

      function onChangeContent( newContent ) {
        props.setAttributes( { content: newContent } );
      }

      function onChangeCite( newCite ) {
        props.setAttributes( { cite: newCite } );
      }

      var retval = [];
      retval.push(
        el(
          RichText,
          {
            tagName: 'div',
            className: 'testimonial-content',
            onChange: onChangeContent,
            value: content,
            isSelected: props.isSelected,
            placeholder: 'Enter testimonial',
          }
        )
      );
      retval.push(
        el(
          RichText,
          {
            tagName: 'div',
            className: 'testimonial-author',
            onChange: onChangeCite,
            value: cite,
            isSelected: props.isSelected,
            placeholder: 'Enter citation',
          }
        )
      );
      return retval;
    },

    save: function( props ) {
      var content = props.attributes.content;
      var cite = props.attributes.cite;
      return el(
        'div',
        { className: 'testimonial' },
        [
          el( 'div', { className: 'testimonial-content' }, content ),
          el( 'cite', { className: 'testimonial-author' }, cite )
        ]
      );
    },
} );
