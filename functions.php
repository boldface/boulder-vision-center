<?php

/**
 * @package Boldface\BoulderVisionCenter
 */
declare( strict_types = 1 );
namespace Boldface\BoulderVisionCenter;

/**
 * Add updater controller to the init hook
 *
 * @since 0.1
 */
function updater_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\updater_init' );

/**
 *
 */
function testimonial( $atts, string $content ) : string {
  $atts = \shortcode_atts( [
    'author' => '',
  ], $atts, 'testimonial' );
  return sprintf(
    '<div class="%1$s"><div class="%2$s">%3$s</div><div class="%4$s">%5$s</div></div>',
    'testimonial',
    'testimonial-content',
    $content,
    'testimonial-author',
    $atts[ 'author' ]
  );
}
\add_shortcode( 'testimonial', __NAMESPACE__ . '\testimonial' );

/**
 * Filter the modules list.
 *
 * @since 0.1
 *
 * @param array $list List of modules.
 *
 * @return array The new list of modules.
 */
function modulesList( array $list ) : array {
  //$list[] = 'featuredImage';
  $list[] = 'gallery';
  $list[] = 'googleFonts';
  $list[] = 'images';
  $list[] = 'openGraph';
  $list[] = 'widgets';
  return $list;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modulesList' );

/**
 *
 */
function body_class( array $class ) : array {
  $class[] = 'bg-light';
  $class[] = 'bvc-blue';
  return $class;
}
\add_filter( 'body_class', __NAMESPACE__ . '\body_class' );

/**
 * Use the wp_enqueue_scripts hook to enqueue the boulder-vision-center stylesheet.
 *
 * @since 0.1
 */
function enqueueScripts(){
  \wp_enqueue_style( 'boulder-vision-center', \esc_url( \get_stylesheet_directory_uri() . '/assets/css/style.css' ), [ 'booter', 'bootstrap' ] );
  \wp_enqueue_script( 'boulder-vision-center-nav', \esc_url( \get_stylesheet_directory_uri() . '/assets/js/nav.js' ), [ 'jquery' ] );
  \wp_enqueue_script( 'boulder-vision-center-appointment', \esc_url( \get_stylesheet_directory_uri() . '/assets/js/appointment.js' ), [ 'jquery' ] );
  \wp_add_inline_style( 'boulder-vision-center', sprintf( '.icon:after{background-image:URL(%1$s);}', \get_stylesheet_directory_uri() . '/assets/images/Boulder-Vision-Center-Icon.svg' ) );
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueueScripts' );

/**
 *
 */
function navigationBrand( string $brand ) : string {
  if( $logo = \get_theme_mod( 'custom_logo' ) ) {
    return sprintf(
      '<img src="%1$s" alt="%2$s" class="img img-fluid img-brand initial" />',
      \wp_get_attachment_image_src( $logo, 'full' )[0],
      \apply_filters( 'Boldface\Bootstrap\Models\navigation\brand\alt', 'Bootstrap Brand Logo' )
    );
  }
  return '';
}
\add_filter( 'Boldface\Bootstrap\Models\navigation\brand', __NAMESPACE__ . '\navigationBrand' );

/**
 *
 */
function navigationMenuArgs( array $args ) : array {
  $args[ 'menu_class' ] .= ' bg-bvc-teal';
  return $args;
}
\add_filter( 'Boldface\Bootstrap\Models\navigation\menu\args', __NAMESPACE__ . '\navigationMenuArgs' );

/**
 * Return the new navigation class.
 *
 * @since 0.1
 *
 * @param string $class The old navigation class.
 *
 * @return string The new navigation class.
 */
function navigationClass( string $class ) : string {
  return 'navbar navbar-expand-lg navbar-dark bg-bvc-teal fixed-top text-uppercase';
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 *
 */
function address_bar() {
  if( ! \is_active_sidebar( 'header' ) ) {
    return;
  }?>
  <div class="container-fluid address-bar bg-bvc-dark-blue text-light text-right">
    <?php \dynamic_sidebar( 'header' ); ?>
  </div>
  <?php
  return;
  printf(
    '<div class="container-fluid address-bar text-right">%1$s</div>',
    '<p>303.443.4545 | 1645 28th Street, Boulder CO | <a href="#">yo</a>'
  );
}
\add_action( 'render_html', __NAMESPACE__ . '\address_bar', 10 );

/**
 * Return the featured image callback.
 *
 * @since 0.1
 *
 * @param callable $callback Unused.
 *
 * @return callable The ffeatured image callback.
 */
function featuredImageCallback( callable $callback ) : callable {
  return __NAMESPACE__ . '\featuredImageWP';
}
//\add_filter( 'Boldface\Bootstrap\Controllers\featuredImage\callback', __NAMESPACE__ . '\featuredImageCallback' );

/**
 *
 */
function featuredImageWP() {
  \add_action( 'Boldface\Bootstrap\Views\loop\start', __NAMESPACE__ . '\featuredImage' );
}

/**
 *
 */
function featuredImage() {
  printf(
    '<div class="featured-image" style="background-image:URL(%1$s);"><h2 class="featured-text">%2$s</h2></div>',
    \get_the_post_thumbnail_url(),
    \get_post_meta( \get_the_ID(), '_featured_text', true )
  );
}

/**
 *
 */
function featuredText( $post ) {
  $meta = \get_post_meta( $post->ID );
  \wp_nonce_field( 'featured_text_nonce', 'featured_text_nonce' );
  $featured_text = \get_post_meta( $post->ID, '_featured_text', true );
  printf( '<textarea name="featured_text">%1$s</textarea>', $featured_text );
}

/**
 *
 */
function add_meta_boxes() {
  \add_meta_box(
    'featured-text',
    __( 'Featured Text', 'boulder-vision-center' ),
    __NAMESPACE__ . '\featuredText',
    'page',
    'side'
  );
}
//\add_action( 'add_meta_boxes', __NAMESPACE__ . '\add_meta_boxes' );

/**
 *
 */
function save_post( $id ) {
  if( ! isset( $_POST[ 'featured_text_nonce' ] ) ) {
    return;
  }
  $nonce = $_POST[ 'featured_text_nonce' ];
  if( ! \wp_verify_nonce( $nonce, 'featured_text_nonce' ) ) {
    return;
  }
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }
  if( ! \current_user_can( 'edit_page', $id ) ) {
    return;
  }
  $data = \sanitize_textarea_field( $_POST[ 'featured_text' ] );
  \update_post_meta( $id, '_featured_text', $data);
}
\add_action( 'save_post', __NAMESPACE__ . '\save_post' );

/**
 * Print the post thumbnail url as background image.
 *
 * @since 0.1
 */
function wp_head() {
  if( ! \has_post_thumbnail() ) return;
  printf( '<style>article.has-post-thumbnail{background-image:URL(%1$s);}</style>', \get_the_post_thumbnail_url() );
}
//\add_action( 'wp_head', __NAMESPACE__ . '\wp_head' );

/**
 * Return the new footer text.
 *
 * @since 0.1
 *
 * @param string $class The old footer text.
 *
 * @return string The new footer text.
 */
function footerText( string $text ) : string {
  $year = date( 'Y' );
  return "<p class=\"text-white text-center mx-auto mb-0\">&copy; $year &mdash; Rocky Mountain Eye Institute LLC</p>";
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 *
 */
function footerWrapperClass( string $class ) : string {
  return 'container-fluid';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\wrapper\class', __NAMESPACE__ . '\footerWrapperClass' );

/**
 * Return the new footer class.
 *
 * @since 0.1
 *
 * @param string $class The old footer class.
 *
 * @return string The new footer class.
 */
function footerClass( string $class ) : string {
  return 'footer navbar navbar-dark bg-bvc-dark-blue';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Return the new loop class.
 *
 * @since 0.1
 *
 * @param string $class The old loop class.
 *
 * @return string The new loop class.
 */
function loopClass( string $class ) : string {
  return 'container-fluid';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Return the content loop class.
 *
 * @since 0.1
 *
 * @param string $class The old content class.
 *
 * @return string The new content class.
 */
function contentClass( string $class ) : string {
  return 'container ' . $class;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\content\class', __NAMESPACE__ . '\contentClass' );

/**
 * Return the new gallery class.
 *
 * @since 0.1
 *
 * @param string $class The old gallery class.
 *
 * @return string The new gallery class.
 */
function galleryClass( string $class ) : string {
  return 'd-flex flex-md-nowrap flex-wrap';
}
\add_filter( 'Boldface\Bootstrap\Views\gallery\class', __NAMESPACE__ . '\galleryClass', 20 );

/**
 * Return the new Google Fonts.
 *
 * @since 0.1
 *
 * @param string $class The old Google Fonts.
 *
 * @return string The new Google Fonts.
 */
function googleFonts( array $fonts ) : array {
  return [ 'Raleway', 'Merriweather Sans' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Add filter to bypass the Bootstrap REST API.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );

/**
 *
 */
function init() {
  if( ! function_exists( 'register_block_type' ) ) {
    return;
  }
  \wp_register_script(
    'bootstrap-testimonial',
    \get_stylesheet_directory_uri() . '/assets/js/gutenstrap.js',
    [ 'wp-blocks', 'wp-element' ]
  );
  \wp_register_style(
    'bootstrap-testimonial',
    \get_stylesheet_directory_uri() . '/assets/css/gutenstrap.css',
    [ 'wp-edit-blocks' ]
  );
  \register_block_type( 'bootstrap/testimonial', [
    'editor_script' => 'bootstrap-testimonial',
    'editor_style'  => 'bootstrap-testimonial',
  ] );
}
\add_action( 'init', __NAMESPACE__ . '\init' );

/**
 *
 */
function appointment() {
  printf(
    '<div class="appointment expand">
      <div class="bg-bvc-red appointment-image"><img src="%1$s"></div>
      <div class="bg-bvc-red appointment-text"><a href="/appointments/">Make an Appointment</a></div>
    </div>',
    \get_stylesheet_directory_uri() . '/assets/images/Boulder-Vision-Center-calendar-icon.svg'
  );
}
\add_action( 'render_html', __NAMESPACE__ . '\appointment', 80 );

/**
 *
 */
function google_map() {
  printf( '<div class="google-map-outer"><div class="google-map-canvas">
    <iframe width="2000" height="500" id="gmap_canvas" src="%1$s" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div></div>', 'https://maps.google.com/maps?q=Boulder+Vision+Center&t=&z=15&ie=UTF8&iwloc=&output=embed' );
}
\add_action( 'render_html', __NAMESPACE__ . '\google_map', 80 );

/**
 *
 */
function footer() {
  if( ! \is_active_sidebar( 'footer' ) ) {
    return;
  }?>
  <div class="container-fluid bg-light bvc-blue flex-wrap flex-lg-nowrap footer-widgets">
    <div class="row">
      <?php \dynamic_sidebar( 'footer' ); ?>
    </div>
  </div>
  <?php
}
\add_action( 'render_html', __NAMESPACE__ . '\footer', 95 );

/**
 *
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'name'          => 'Footer',
    'id'            => 'footer',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
  ];
  $sidebars[] = [
    'name'          => 'Header',
    'id'            => 'header',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
  ];
  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 *
 */
function button_class( string $class ) : string {
  return str_replace( 'btn-primary', 'btn-secondary', $class );}
\add_filter( 'Boldface\Bootstrap\Models\contactForm7\button\class', __NAMESPACE__ . '\button_class' );

/**
 *
 */
\add_filter( 'Boldface\Bootstrap\Controllers\entry\autorow', '__return_false' );
