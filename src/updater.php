<?php

/**
 * @package Boldface\BoulderVisionCenter
 */
declare( strict_types = 1 );
namespace Boldface\BoulderVisionCenter;

/**
 * Class for updating the theme
 *
 * @since 0.1
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'boulder-vision-center';
  protected $repository = 'boldface/boulder-vision-center';
}
